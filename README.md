
# Daniel Saxil-Nielsen

## Fixes
- `api.johnlewis.com` blocks requests which aren't made by a browser. This includes requests from NextJS. You can see this by curling the URL. The fix is to spoof a browser when making this request.
- The provided mock data was out of date compared with the current API. `item.price` is now `variantPriceRange`. I've just taken `variantPriceRange.display.max` as the current price. Updated the mock results.
- @testing-library/react no longer supports React versions below 17. Had to downgrade to 12.1.2
- `node-sass` is no longer supported, and doesn't work on Apple Silicon. Updated to `sass`.
- Installed `identity-obj-proxy`, it's required for module mocks in Jest
- Added the `__mocks__` directory and the `styleMock.js` export for when we are using css (via the carousel).

## Third party components

### Carousel

I'm using `react-responsive-carousel` to provide the carousel, building a responsive carousel would take more than the recommended number of hours so it makes sense to use a 3rd party one.
