import Head from "next/head";
import styles from "./index.module.scss";
import ProductListItem from "../components/product-list-item/product-list-item";

export async function getServerSideProps() {
	try {
		const response = await fetch(
			"https://api.johnlewis.com/search/api/rest/v2/catalog/products/search/keyword?q=dishwasher&key=AIzaSyDD_6O5gUgC4tRW5f9kxC0_76XRC8W7_mI",
			{
				headers: {
					"Content-Type": "application/json",
					// mock the user agent because this API doesn't like requests from the nextjs server
					"user-agent":
						"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36",
				},
			}
		);
		const data = await response.json();
		return {
			props: {
				data,
				error: false
			},
		};
	} catch (e) {
		// unknown error returned
		// log e to a data logger
		return {
			props: {
				data: false,
				error: true
			},
		};
	}
}

const Home = ({ data, error }) => {
	return (
		<div>
			<Head>
				<title>JL &amp; Partners | Home</title>
				<meta name="keywords" content="shopping" />
			</Head>
			<div>

				{error && (
					<p>Unfortunately we have encounted an error, please try again</p>
				)}

				{!error && data?.products.length > 0 && (
					<>
						<h1>Dishwashers ({data.products.length})</h1>
						<div className={styles.content}>
							{data.products.map((item) => (
								<ProductListItem
									id={item.productId}
									image={item.image}
									title={item.title}
									price={item.variantPriceRange.display.max}
								/>
							))}
						</div>
					</>
				)}

				{!error && data?.products.length === 0 && (
					<h1>There are no dishwashers</h1>
				)}
			</div>
		</div>
	);
};

export default Home;
