import ProductCarousel from "../../components/product-carousel/product-carousel";
import styles from './product-detail.module.scss';

/**
 * Fetch the data for this item from the API
 *
 * @export
 * @param {*} context
 * @return {Object} {props: { data, error }}
 */
export async function getServerSideProps(context) {
	const id = context.params.id;
	try {
		const response = await fetch("https://api.johnlewis.com/mobile-apps/api/v1/products/" + id, {
			headers: {
				"Content-Type": "application/json",
				"user-agent":
					"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36",
			},
		});
		const data = await response.json();
		return {
			props: {
				error: false,
				data,
			},
		};
	} catch (e) {
		return {
			props: {
				error: true
			}
		}
	}
}

const ProductDetail = ({ data, error }) => {

	if (error) {
		return <p>Unfortunately, we have encounted an error loading this product. Please try again.</p>;
	}

	const pricing = (
		<>
			<div className={styles.price}>£{data.price.now}</div>
			<div className={styles.special}>{data.displaySpecialOffer}</div>
			<div className={styles.included}>{data.additionalServices.includedServices}</div>
		</>
	)
	return (
		<>
			<h1>{data.title}</h1>
			<div className={styles.page}>
				<div className={styles.productLeft}>
					<ProductCarousel image={data.media.images.urls} />

					<article className={styles.information}>
						<h3 className={styles.pricingPanelMobile}>
							{pricing}
						</h3>
						<div>
							<h3>Product information</h3>
							{/* Unfortunately, the API is returning HTML and this is the main way we render
							HTML in React. In an ideal world the API should be returning an object so we
							can build the HTML from the object. */}
							<div dangerouslySetInnerHTML={{__html: data.details.productInformation}} />
						</div>
						<div>
							<p>Product code: {data.defaultSku}</p>
						</div>
						<h3>Product specification</h3>
						<ul className={styles.list}>
							{data.details.features[0].attributes.map((item, i) => (
								<li key={i} className={styles.listItem}>
									<div>{item.name}</div>
									<div>{item.value}</div>
								</li>
							))}
						</ul>
					</article>
				</div>
				<div className={styles.productRight}>
					<h3 className={styles.pricingPanelDesktop}>
						{pricing}
					</h3>
				</div>
			</div>
		</>
	);
};

export default ProductDetail;
