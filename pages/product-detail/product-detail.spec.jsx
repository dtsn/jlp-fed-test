import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import data from '../../mockData/data2.json';
import ProductDetail, { getServerSideProps } from "./[id]";

describe('Product Grid', () => {

	beforeEach(() => {
		fetch.resetMocks();
	});

	it('should load the item', async () => {
		// mock the API, ensure we get the items back in the correct format
		fetch.mockResponseOnce(JSON.stringify(data));
		const response = await getServerSideProps({params: {id: 1}});
		expect(response).toEqual({
			props: {
				error: false,
				data: data
			}
		});
	});

	it('when it errors we should show a message', async () => {
		// passing a resposne which isn't json will cause an error
		fetch.mockResponseOnce('hello world');
		const response = await getServerSideProps({params: {id: 1}});
		expect(response).toEqual({
			props: {
				error: true,
			}
		});

		render(<ProductDetail error />);
		const errorMessage = screen.getByText('Unfortunately, we have encounted an error loading this product. Please try again.');
		expect(errorMessage).toBeInTheDocument();
	});

	it('shows the correct price', async () => {
		render(<ProductDetail data={data} />);
		const price = screen.getAllByText(`£${data.price.now}`);
		expect(price[0]).toBeInTheDocument();
	});
});
