import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import data from '../mockData/data.json';
import Home, { getServerSideProps } from "./index";

describe('Product Grid', () => {

		beforeEach(() => {
			fetch.resetMocks();
		});

		it('should load all the items', async () => {
			// mock the API, ensure we get the items back in the correct format
			fetch.mockResponseOnce(JSON.stringify(data));
			const response = await getServerSideProps();
			expect(response).toEqual({
				props: {
					error: false,
					data: data
				}
			});
		});

		it('when it errors we should show a message', async () => {
			// passing a resposne which isn't json will cause an error
			fetch.mockResponseOnce('hello world');
			const response = await getServerSideProps();
			expect(response).toEqual({
				props: {
					error: true,
					data: false
				}
			});

			render(<Home error />);
			const errorMessage = screen.getByText('Unfortunately we have encounted an error, please try again');
			expect(errorMessage).toBeInTheDocument();
		});

		it('when there are 0 items, show a message', async () => {
			render(<Home data={{products: []}} />);
			const errorMessage = screen.getByText('There are no dishwashers');
			expect(errorMessage).toBeInTheDocument();
		})
});
